### Deploying to Kubernetes Cluster



#### Setting up the Voting Application
* Have a look at kubernetes specs for the vote app
   ```
   cd ~/example-voting-app
   ```
   ```
   ls k8s-specifications
   ```
* Folder contains specification files for 
   + Deployments
   + Services


#### Creating a namespace
* <!-- .element: class="fragment" data-fragment-index="0" -->In dashboard click *Projects/Namespaces*
* <!-- .element: class="fragment" data-fragment-index="1" -->In the *Project Default* section, click *Add Namespace*
* <!-- .element: class="fragment" data-fragment-index="2" -->On next page
  enter **vote** in *Name* field and click *Create*
* <!-- .element: class="fragment" data-fragment-index="3" -->This creates a new namespace in your cluster
* <!-- .element: class="fragment" data-fragment-index="4" -->Can confirm by running in the shell
   ```
   kubectl get ns
   ```


#### Watch cluster
* <!-- .element: class="fragment" data-fragment-index="0" -->A couple ways to watch what is happening
* <!-- .element: class="fragment" data-fragment-index="1" -->In your terminal
   ```
   watch kubectl -n vote get all
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->In the dashboard
  - In *Project/Namespaces* menu
  - click on *Project: Default*
  - click on *Workloads* tab


#### Load Specification Files
<code>kubectl </code><code style="color:blue;">apply</code><code style="color:green;"> -f specfile.yml</code>

* The <!-- .element: class="fragment" data-fragment-index="0" -->`apply` command _applies_ a configuration to a specific resource
*  <!-- .element: class="fragment" data-fragment-index="1" -->The entire vote app is specified in yaml files
   ```
   cd ~/example-voting-app
   kubectl apply -n vote -f k8s-specifications
   ```
* <!-- .element: class="fragment" data-fragment-index="2" -->This tells Kubernetes to begin setting up containers
   + creates network endpoints
   + assigns Pods to replication controller
* When you run this, go back to the <!-- .element: class="fragment" data-fragment-index="3" -->_watcher_ terminal



#### Viewing Vote Website
* At the moment we have no way to view the website
* Need to set up a service so that we can reach site


#### Summary
* In this section we deployed a microservice application
* kubectl can scale services up or down
* Deployment rolling updates ensure that the application updates seemlessly
  with zero downtime
